import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap , Params } from '@angular/router';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.page.html',
  styleUrls: ['./task.page.scss'],
})
export class TaskPage implements OnInit {
  listDriver: any[];
  listTask: any[];
  mydata: any;
  affichage :any;
  keysend : string ;
  selectedId: any;

  constructor( 
    private authService :AuthService,
    private routeur : Router,
    private activatedRoute : ActivatedRoute) 
    { 
      
    }

  ngOnInit() {
    this.afficheRequette() ;


   

  }

  map(key) {
    
  //   this.route.params.subscribe(params => {
  //     const id = Number.parseInt(params['paramKey']);
  //  })



      // this.routeur.navigate(['map']);
      // this.keysend =  key  ;
      // console.log("key parent "+this.keysend);
      // this.routeur.navigate(['/heroes', { id: itemId }]);
      
  
  }
  
  afficheRequette() {
    this.authService.listTask().subscribe(
      list => {
        this.listTask = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()

          }
        });
      
      // this.mydata = JSON.stringify(this.listTask);
      this.mydata = this.listTask;
      // console.log("Liste des drivers : "+this.mydata);
    });

    
  }

}
