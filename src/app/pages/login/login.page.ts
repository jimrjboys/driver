import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { AngularFireDatabase } from 'angularfire2/database';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  data : any ; 
  errorMessage: any;
  data1: any;
  listDriver: any[];
  constructor(private authService :AuthService,
    private  router:  Router , ) { }

  ngOnInit() {
    console.log('initialisation');
  this.ini();
  
  }
  ini(){

    this.authService.listDriver().subscribe(
      list => {
        this.listDriver = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()

          }
        });
      console.log("Liste des drivers : "+this.listDriver);
    });

  
  }
  connexion(form){

    console.log(" les liste  " +this.listDriver);
    this.data = form.value.email;
      this.data1 = form.value.password;
      console.log (this.data);
      
      this.authService.signInUser(this.data, this.data1).then(
        () => {
    
          this.router.navigate(['menu']);

          
        },
        (error) => {
          this.errorMessage = error;
          console.log(this.errorMessage);
        }
      );

    
  }

}
