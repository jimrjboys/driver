import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  constructor(    private  router:  Router , ) { }

  ngOnInit() {
  }
  task (){
    this.router.navigate(['task']);
    
  }
  task_online(){
    this.router.navigate(['appointment']);
  }
}
