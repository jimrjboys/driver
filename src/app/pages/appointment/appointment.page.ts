import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap , Params } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.page.html',
  styleUrls: ['./appointment.page.scss'],
})
export class AppointmentPage implements OnInit {
  listTaskOnline: any[];
  mydata: any;
 

  constructor(   
     private authService :AuthService,
    private routeur : Router,
    private activatedRoute : ActivatedRoute) { }

  ngOnInit() 
  {
    this.affiche();
  }
    
  affiche() {
    this.authService.listTaskOnline().subscribe(
      list => {
        this.listTaskOnline = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()

          }
        });
      
      // this.mydata = JSON.stringify(this.listTask);
      this.mydata = this.listTaskOnline;
      // console.log("Liste des drivers : "+this.mydata);
    });

  }
}
