import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as  PolylineUtil from "polyline-encoded";
import * as L from 'leaflet';
import {Map,tileLayer,marker} from 'leaflet';
import { MapService } from 'src/app/map.service';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-map1',
  templateUrl: './map1.page.html',
  styleUrls: ['./map1.page.scss'],
})
export class Map1Page implements OnInit {
  params: any;
  
  map:Map;
  newMarker: any;
  result: any;
  distance: any;
  temps: any;
  key : any ;
  listTask: any;
  mydata: any;
  final: any;
  latD: any;
  lngD: any;
  lat: any;
  long: any;
  listClient: any;
  mydata1: any;
  nom: any;
  phone: any;
  final1: any;
  date: any;
  dateCreation: any;
  heure: any;
  latA: any;
  longA: any;

  constructor(
    private router : Router ,
      private route: ActivatedRoute,
      private MyMap : MapService ,
       private authService :AuthService,
      
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.key = params['id'];

  });

  }
  
  ionViewDidEnter() {
    this.loadMap(); //  chargement de la carte
  }
  ionViewWillLeave() {
    this.map.remove(); //dechargement de la carte
    this.key = 0 ;
  }

  loadMap(){
    console.log(this.key) ;
 
    this.map = new Map("mapId").setView([-18.91368, 47.53613], 13);tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
      attribution:
        'Umblah © <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
    }).addTo(this.map);

    //recuperation des informations du client

    this.authService.listClient().subscribe(
      list => {
        this.listClient = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          }
        });
       const id = this.key;
       console.log(this.listClient);
       this.final = this.listClient.filter(function (e) {
        return (e.$key == id);
      });
      console.log(this.final);
      this.mydata1 = JSON.stringify(this.final) ;
      let data1 = JSON.parse(this.mydata1);
      console.log(data1[0].Nom,data1[0].Numero);
      this.nom = data1[0].Nom ; 
      this.phone = data1[0].Numero;
 
  });


  //recuperation des information de taches
    this.authService.listTaskOnline().subscribe(
      list => {
        this.listTask = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          }
        });     

      const id = this.key;
    this.final = this.listTask.filter(function (e) {
      return (e.$key == id);
    });

    this.mydata = JSON.stringify(this.final) ;
    let data = JSON.parse(this.mydata);
  
    this.heure = data[0].heure ;
    this.date =  data[0].date;
    this.dateCreation = data[0].date_creation;
    // var split  = data[0].latD.split(" ");
    this.latD =data[0].latD;
    this.lngD = data[0].lngD;
    this.latA = data[0].latA;
    this.longA =data[0].lng;
    this.temps =data[0].duree;
    this.distance =data[0].distance ; 

    // this.newMarker = L.marker([this.latD,this.lngD], {
    //   draggable: false,
      
    // }).addTo(this.map);
    // this.newMarker = L.marker([this.lat,this.long], {
    //   draggable: false,

      
    // }).addTo(this.map);
    L.marker([this.latD,this.lngD], ).addTo(this.map).bindPopup("Votre Client est ici").openPopup();
    L.marker([this.latA,this.longA]).addTo(this.map).bindPopup("Sa destinnation").openPopup();
    this.affichage( this.latD ,this.lngD,this.latA,this.longA);
    });
  
  }
  affichage (latD , lngD ,latA ,lngA){

    console.log(latD , lngD ,latA ,lngA);
    this.latD = latD.toFixed(8);
    this.lngD = lngD.toFixed(8);
    this.latA =latA.toFixed(8);
    this.longA =lngA.toFixed(8);
    this.MyMap.itineraire( this.latD ,this.lngD,this.latA ,this.longA).subscribe(datas => {
      console.log("tracace en cours");
      this.result = JSON.stringify(datas);
      let result = JSON.parse(this.result);
      var recup = result.polyline;
      var encoded : any  = recup;
      var polyline : any = PolylineUtil.decode(encoded,6);
      var mypolyline = L.polyline(polyline, {color: 'red'}).addTo(this.map);
      this.map.fitBounds(mypolyline.getBounds());

    });
  }
  ok(){
    const id = this.key;
    this.authService.okTaskOnline(id);
    console.log("tache bien accepté");
  }
  goBack(){

    this.router.navigate(['appointment']);

  }

}
