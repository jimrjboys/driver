import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isAuth = false;
  iud  : any ; 
  driver: AngularFireList<any>;
  task: AngularFireList<any>;
  client: AngularFireList<any>;
  taskOnline: any;
  constructor( private afDB :AngularFireDatabase ,private fb_update: AngularFireDatabase ) {

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
      this.isAuth = true;
      } else {
      this.isAuth = false;
      }
      });
   }
 signInUser(email: string, password: string) {
  return new Promise((resolve, reject) => {
    firebase.auth().signInWithEmailAndPassword(email, password).then(
      (user) => {
        resolve(user);
        this.isAuth = true;
      },
      (error) => {
        reject(error);
      }
    );
  });
}
signOut() {
  firebase.auth().signOut();
}
listDriver() {
  this.driver = this.afDB.list('driver');
  console.log('provider de chargement activer liste des driver');
  return this.driver.snapshotChanges();

}
listTask() {
  this.task = this.afDB.list('task');
  console.log('provider de chargement activer liste des taches ');
  return this.task.snapshotChanges();

}
listTaskOnline() {
  this.taskOnline = this.afDB.list('Task_online');
  console.log('provider de chargement activer liste des taches ');
  return this.taskOnline.snapshotChanges();

}
listClient(){

  this.client = this.afDB.list('account');
  console.log('provider de chargement activer liste des Clients ');
  return this.client.snapshotChanges();

}
okTask(id){
  var new_etat = "accepte"
  return new Promise((resolve) => {
    let $ref = this.fb_update.list(`task`)
    console.log($ref)
    $ref.update("/"+id+"/", {
      Etat:new_etat
    })
    .then(() => {
      resolve()
    }, (error) => {
      console.error(error)
    })
  })

}
okTaskOnline(id){
  var new_etat = "accepte"
  return new Promise((resolve) => {
    let $ref = this.fb_update.list(`Task_online`)
    console.log($ref)
    $ref.update("/"+id+"/", {
      Etat:new_etat
    })
    .then(() => {
      resolve()
    }, (error) => {
      console.error(error)
    })
  })

}
}
