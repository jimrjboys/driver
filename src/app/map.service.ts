import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MapService {


  constructor(private http : HttpClient) { }
  itineraire(latD:any , lngD :any ,latA: any , longA: any){

    console.log('Chargement de l api ok');
    return this.http.get("https://maps.open-street.com/api/route/?origin="+latD+","+lngD+"&destination="+latA+","+longA+"&mode=driving&key=e206e1442a767916d102965ba0bdb76c");
    
    
  }
}
