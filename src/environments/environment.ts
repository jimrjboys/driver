// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase :{
    apiKey: "AIzaSyAUdXj0KNdAMebCygFuEMwaiVhv1O7fVQQ",
    authDomain: "pikla-a233a.firebaseapp.com",
    databaseURL: "https://pikla-a233a.firebaseio.com",
    projectId: "pikla-a233a",
    storageBucket: "pikla-a233a.appspot.com",
    messagingSenderId: "703065329968",
    appId: "1:703065329968:web:53657cf09d798f5afff8f5",
    measurementId: "G-EVTNKKX9XT"
  
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
